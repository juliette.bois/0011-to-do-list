# To Do List

TP de jQuery dans le cadre de mes études à l'IPI de Lyon (Bac+3 Concepteur Développeur Full-Stack)

## Pour faire fonctionner le projet
1. cloner le projet
2. se rendre à la racine du projet et faire un `npm install`
3. faire un `npm run watch`
4. ouvrir l' `index.html` (to-do-list/dist/index.html)


## Besoin
Un utilisateur peut **se créer un compte** ou **se connecter** s'il possède déja un compte.
En créant son compte, cela initialise la BDD. Il y a par défaut 2 TodoListes.

## Fonctionnalités
L'utilisateur doit pouvoir :
- ajouter un élément à une liste
- supprimer un élément à une liste
- modifier un élément à une liste (= modifier une todoliste)
- ajouter une nouvelle todoliste
- supprimer une todoliste

## Outils utilisés
HTML & SCSS, Librairie jQuery, JavaScript (ECMANext) pour moduler mon code JavaScript, Laravel-Mix (pour compiler le SCSS & le JS)



Le tout avec des animations sympas du type FadeIn, FadeOut afin de rendre l'app plus "fluide".