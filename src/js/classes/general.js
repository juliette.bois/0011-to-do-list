import axios from 'axios';
import $ from 'jquery';

export default class General {
	constructor () {
		this.initElements();
		this.initEvents();
	}

	initElements () {
		this.API_URL = 'http://92.222.69.104:80';
	}

	initEvents () {
		this.onClickRegisterFormLink();
		this.onClickLoginFormLink();
		this.onSubmitRegisterForm();
		this.onSubmitLoginForm();
		this.onKeyPressedAddItemInput();
		this.onClickDeleteItemButton();
		this.onClickEditItemButton();
		this.onClickAddTodolistButton();
		this.onClickDeleteTodolistButton();
	}


	/*
		**** Évènements ****
	*/
	onClickRegisterFormLink() {
		$('header span.register').on('click', () => {
			$('section#login').hide();
			$('section#register').fadeIn(500, () => {
				$('section#register').show();
			});
		});
	}

	onClickLoginFormLink() {
		$('header span.login').on('click', () => {
			$('section#register').hide();
			$('section#login').fadeIn(500, () => {
				$('section#login').show();
			});
		});
	}

	onSubmitRegisterForm() {
		$('section#register form').on('submit', (event) => {
			event.preventDefault();
			let credentials = {};
			credentials.login = $('section#register input[type="text"]').val();
			credentials.password = $('section#register input[type="password"]').val();

			this.registerApi(credentials)
			.then((response) => {
				$('section#register').hide();
				$('section#login').fadeIn(500, () => {
					$('section#login').show();
				});
			})
			.catch((error) => {
				$('section#register span.error').text('Une erreur s\'est produite, veuillez réessayer plus tard.');			});
		});
	}

	onSubmitLoginForm() {
		$('section#login form').on('submit', (event) => {
			event.preventDefault();
			let credentials = {};
			credentials.login = $('section#login input[type="text"]').val();
			credentials.password = $('section#login input[type="password"]').val();

			this.setCredentialsFromSession(credentials);

			this.loginApi(credentials)
			.then((response) => {
				let data = response.data;
				this.setTodolistsFromSession(data);

				this.displayTodolists();
				$('section#login').hide();
				$('header nav').hide();
				$('section#lists').fadeIn(500, () => {
					$('section#lists').show();
				});
				$('section#lists h1.welcome').text(`Bienvenue ${credentials.login} 👋`);
			})
			.catch((error) => {
				$('section#register span.error').text('Vos identifiants sont incorrects.');
			});
		});
	}

	onKeyPressedAddItemInput() {
		$(document).on('keypress', 'input.add_item', (event) => {
			if(event.keyCode === 13) {
				let value = $(event.currentTarget).val();
				let indexTodolist = $(event.currentTarget).parent().index();
				this.addItemToTodolist(indexTodolist, value)
				.then(() => {
					$(event.currentTarget).parent().find('ul').append(`
						<li>${value}<div class"change"><span class="edit_item"><img src="./images/edit.svg" alt="Modifer"></span><span class="delete_item"><img src="./images/delete.svg" alt="Supprimer"></span></div></li>
					`);
					$(event.currentTarget).val('');
				})
				.catch(() => {

				});
			}
		});
	}

	onClickDeleteItemButton() {
		$(document).on('click', 'span.delete_item', (event) => {
			let indexItem = $(event.currentTarget).parent().parent().index();
			let indexTodolist = $(event.currentTarget).parent().parent().parent().parent().index();
			this.deleteItemFromTodolist(indexTodolist, indexItem)
			.then(() => {
				$(event.currentTarget).parent().parent().remove();
			})
			.catch(() => {

			});
		});
	}

	onClickEditItemButton() {
		$(document).on('click', 'span.edit_item', (event) => {
			let old_value = $(event.currentTarget).parent().parent().text();
			let new_value = prompt('Modifer l\'élément', old_value);
			let indexTodolist = $(event.currentTarget).parent().parent().parent().parent().index();
			let indexItem = $(event.currentTarget).parent().parent().index();
			this.editItemFromTodolist(indexTodolist, indexItem, new_value)
			.then(() => {
				$(event.currentTarget).parent().parent().html(
					`${new_value}<div class"change"><span class="edit_item"><img src="./images/edit.svg" alt="Modifer"></span><span class="delete_item"><img src="./images/delete.svg" alt="Supprimer"></span></div>`
				);
			})
			.catch(() => {

			});
		});
	}

	onClickAddTodolistButton() {
		$('span.add_list').on('click', () => {
			let name = prompt('Ajouter une liste');
			this.addTodolist(name)
			.then(() => {
				$('section#lists div.wrap div.lists').append(`
					<div class="list">
						<h2>${name}<span class="delete_list"><img src="./images/delete.svg" alt="Supprimer"></span></li></h2>
						<ul>
						</ul>
						<input class="add_item" type="text" placeholder="Ajouter un élément">
					</div>
				`);
			})
			.catch(() => {

			});
		});
	}

	onClickDeleteTodolistButton() {
		$(document).on('click', 'span.delete_list', (event) => {
			let indexTodolist = $(event.currentTarget).parent().parent().index();
			this.deleteTodolist(indexTodolist)
			.then(() => {
				$(event.currentTarget).parent().parent().remove();
			})
			.catch(() => {

			});
		});
	}
	/**********************/



	/*
		**** Méthodes pour tout ce qui concerne l'API ****
	*/
	loginApi(credentials) {
		return axios.get(`${this.API_URL}/todo/listes`, {
			headers: {
				login: credentials.login,
				password: credentials.password,
			}
		});
	}

	registerApi(credentials) {
		return axios.get(`${this.API_URL}/todo/create/${credentials.login}/${credentials.password}`);
	}

	updateTodolistApi(data) {
		return axios.post(`${this.API_URL}/todo/listes`, data);
	}
	/***************************/



	/*
		**** Getters / Setters ****
	*/
	getTodolistsFromSession() {
		return JSON.parse(sessionStorage.getItem('data'));
	}

	setTodolistsFromSession(data) {
		sessionStorage.setItem('data', JSON.stringify(data));
	}

	getCredentialsFromSession() {
		return JSON.parse(sessionStorage.getItem('credentials'));
	}

	setCredentialsFromSession(credentials) {
		sessionStorage.setItem('credentials', JSON.stringify(credentials));
	}
	/***************************/



	/*
		**** Méthodes pour tout ce qui concerne les listes (afficher, ajouter, supprimer, modifier) ****
	*/
	displayTodolists() {
		let html = '';
		let data = this.getTodolistsFromSession();
		data.todoListes.forEach((element) => {
			html += this.createTodolistElement(element);
		});
		$('section#lists > div.wrap div.lists').html(html);
	}

	createTodolistElement(todoList) {
		let html = `<div class="list">`;
			html += `<h2>${todoList.name}<span class="delete_list"><img src="./images/delete.svg" alt="Supprimer"></span></li></h2>`;
			html += '<ul>';
			todoList.elements.forEach(item => {
				html += `<li>${item}<div class"change"><span class="edit_item"><img src="./images/edit.svg" alt="Modifer"></span><span class="delete_item"><img src="./images/delete.svg" alt="Supprimer"></span></div></li>`;
			});
			html += '</ul>';
			html += '<input class="add_item" type="text" placeholder="Ajouter un élément">';
			html += '</div>';

		return html;
	}

	addItemToTodolist(indexTodolist, value) {
		let data = this.getTodolistsFromSession();
		data.todoListes[indexTodolist].elements.push(value);
		this.setTodolistsFromSession(data);
		return this.updateTodolistApi(data);
	}

	deleteItemFromTodolist(indexTodolist, indexItem) {
		let data = this.getTodolistsFromSession();
		data.todoListes[indexTodolist].elements = data.todoListes[indexTodolist].elements.slice(0, indexItem).concat(data.todoListes[indexTodolist].elements.slice(indexItem + 1, data.todoListes[indexTodolist].elements.length));
		this.setTodolistsFromSession(data);
		return this.updateTodolistApi(data);
	}

	editItemFromTodolist(indexTodolist, indexItem, new_value) {
		let data = this.getTodolistsFromSession();
		data.todoListes[indexTodolist].elements[indexItem] = new_value;
		this.setTodolistsFromSession(data);
		return this.updateTodolistApi(data);
	}

	addTodolist(name) {
		let data = this.getTodolistsFromSession();
		data.todoListes.push({
			name: name,
			elements: []
		});

		this.setTodolistsFromSession(data);
		return this.updateTodolistApi(data);
	}

	deleteTodolist(indexTodolist) {
		let data = this.getTodolistsFromSession();
		data.todoListes = data.todoListes.slice(0, indexTodolist).concat(data.todoListes.slice(indexTodolist + 1, data.todoListes.length));
		this.setTodolistsFromSession(data);
		return this.updateTodolistApi(data);
	}
	/***************************/
}